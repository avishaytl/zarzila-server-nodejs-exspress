"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAdvertisements = exports.insertInto = exports.getStorie = exports.updateStorie = exports.createStorie = exports.getStoriesList = exports.verifyLogin = exports.connectDB = void 0;
const connectDB = (req, res, isRequest) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const mysql = require('mysql');
        let connection = yield mysql.createConnection({
            host: `localhost`,
            user: 'presolu3_admin',
            password: '400x120x',
            database: 'presolu3_zarzila'
        });
        yield connection.connect(function (err) {
            if (err) {
                console.error('error connecting: ' + err.stack);
                // return;
            }
            console.log('connected as id ' + connection.threadId);
        });
        if (isRequest)
            yield connection.end(function (err) {
                if (err) {
                    res.status(400).json({ code: 400, msg: 'end connection error' });
                    throw err;
                }
                ;
                console.debug('end connection connectDB');
                res.status(200).json({ code: 200, response: 'The DB Is Ready To Use' });
            });
        else
            return connection;
    }
    catch (error) {
        throw error;
    }
});
exports.connectDB = connectDB;
const verifyLogin = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let connection = yield connectDB(req, res, false);
        let user, pass;
        let errMsg = '';
        let errCode = 'v1$23l4-';
        let isVerifyed = false;
        for (const key in req.query) {
            const element = req.query[key];
            if (key === 'user')
                user = element;
            if (key === 'pass')
                pass = element;
        }
        console.debug('verifyLogin req', user, pass);
        if (pass && pass !== undefined && user && user !== undefined)
            yield connection.query(`select userName from users where userName='` + user + `' and userPass='` + pass + `'`, function (error, result, fields) {
                if (error) {
                    errMsg = error.sqlMessage;
                    errCode += 'select from';
                    console.debug('verifyLogin error, ' + error.sqlMessage);
                }
                ;
                if (result && result[0] && result[0].userName === user)
                    isVerifyed = true;
                console.debug('verifyLogin res', result);
                // connected!
            });
        yield connection.end(function (err) {
            if (err) {
                res.status(400).json({ code: 400, msg: 'end connection error' });
                throw err;
            }
            ;
            console.debug('end connection verifyLogin');
            if (errMsg === '')
                res.status(200).json({ code: 200, response: isVerifyed });
            else
                res.status(400).json({ code: 400, msg: 'verifyLogin error, ' + errMsg, errCode: errCode });
        });
    }
    catch (error) {
        throw error;
    }
});
exports.verifyLogin = verifyLogin;
const getAdvertisements = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let connection = yield connectDB(req, res, false);
        let user, pass;
        let errMsg = '';
        let errCode = '1$23a4-';
        let isCreated = true;
        let resData = [];
        yield connection.query(`select * from advertisements`, function (error, result, fields) {
            if (error) {
                errMsg = error.sqlMessage;
                errCode += 'select from';
                isCreated = false;
                console.debug('getAdvertisements error, ' + error.sqlMessage);
            }
            ;
            console.debug('getAdvertisements res', result);
            if (result)
                resData.push(result);
            // connected!
        });
        yield connection.end(function (err) {
            if (err) {
                res.status(400).json({ code: 400, msg: 'end connection error' });
                throw err;
            }
            ;
            console.debug('end connection getAdvertisements');
            if (errMsg === '')
                res.status(200).json({ code: 200, response: isCreated, data: resData });
            else
                res.status(400).json({ code: 400, msg: 'getAdvertisements error, ' + errMsg, errCode: errCode });
        });
    }
    catch (error) {
        throw error;
    }
});
exports.getAdvertisements = getAdvertisements;
const getStorie = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let connection = yield connectDB(req, res, false);
        let errMsg = '';
        let isCreated = true;
        let errCode = 'g1$23sd4-';
        let resData = [];
        let tables = [];
        let tableName = '';
        for (const key in req.query) {
            const element = req.query[key];
            if (key === 'tableName')
                tableName = element;
        }
        console.debug('getStorie req', tableName);
        yield connection.query(`select * from ${tableName}`, function (error, result, fields) {
            if (error) {
                errMsg = error.sqlMessage;
                isCreated = false;
                errCode += 'select from';
                console.debug('getStorie error, ' + error);
            }
            ;
            console.debug('getStorie res', result);
            if (result)
                resData.push(result);
            // connected!
        });
        console.debug('resData', resData);
        yield connection.end(function (err) {
            if (err) {
                res.status(400).json({ code: 400, msg: 'end connection error' });
                throw err;
            }
            ;
            console.debug('end connection getStorie');
            if (errMsg === '')
                res.status(200).json({ code: 200, response: isCreated, data: resData });
            else
                res.status(400).json({ code: 400, msg: 'getStorie error, ' + errMsg, errCode: errCode });
        });
    }
    catch (error) {
        throw error;
    }
});
exports.getStorie = getStorie;
const getStoriesList = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let connection = yield connectDB(req, res, false);
        let errMsg = '';
        let isCreated = true;
        let errCode = 'g1$23sd4-';
        let resData = [];
        let tables = [];
        console.debug('getStoriesData req');
        yield connection.query(`SELECT table_name FROM information_schema.tables WHERE table_schema ='presolu3_zarzila'`, function (error, result, fields) {
            if (error) {
                errMsg = error.sqlMessage;
                errCode += 'select from';
                isCreated = false;
                console.debug('getStoriesData error, ' + error.sqlMessage);
            }
            ;
            console.debug('getStoriesData res', result);
            if (result)
                tables = result;
            // connected! 
        });
        yield connection.end(function (err) {
            if (err) {
                res.status(400).json({ code: 400, msg: 'end connection error' });
                throw err;
            }
            ;
            console.debug('end connection getStoriesData');
            let respose = [];
            // let response: Blog[] = [{
            //     title: 'note title',
            //     description: 'note description' 
            // }]
            for (let i = 0; i < tables.length; i++)
                if (tables[i].table_name !== 'users' && tables[i].table_name !== 'advertisements')
                    respose.push(tables[i]);
            if (errMsg === '')
                res.status(200).json({ code: 200, response: isCreated, data: respose });
            else
                res.status(400).json({ code: 400, msg: 'getStoriesData error, ' + errMsg, errCode: errCode });
        });
    }
    catch (error) {
        throw error;
    }
});
exports.getStoriesList = getStoriesList;
const updateStorie = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let connection = yield connectDB(req, res, false);
        let errMsg = '';
        let errCode = 'u1$23s4-';
        let isCreated = true;
        let mainTitle, subTitle, mainText, subText, category, createdAt, readTime, img, index, title, tableName;
        console.debug('updateStorie req', req.body);
        if (req.body && req.body.Storie_Data && req.body.Storie_Data[0]) {
            let data = req.body.Storie_Data[0];
            mainTitle = data.mainTitle.split("_jg_").join('"');
            subTitle = data.subTitle.split("_jg_").join('"');
            mainText = data.mainText.split("_jg_").join('"');
            subText = data.subText.split("_jg_").join('"');
            category = data.category;
            createdAt = data.createdAt;
            readTime = data.readTime;
            img = data.img;
            title = data.title;
            index = data.index;
            tableName = data.tableName;
        }
        let mainTitleRows = yield JSON.parse(mainTitle);
        let subTitleRows = yield JSON.parse(subTitle);
        let mainTextRows = yield JSON.parse(mainText);
        let subTextRows = yield JSON.parse(subText);
        let response;
        yield connection.query(`DROP TABLE ${tableName}`, yield function (error, result, fields) {
            return __awaiter(this, void 0, void 0, function* () {
                if (error) {
                    errMsg = error.sqlMessage;
                    errCode += 'update into';
                    isCreated = false;
                    console.debug('updateStorie update error, ' + error.sqlMessage);
                }
                ;
                console.debug('updateStorie update res', isCreated, result);
                // connected!
            });
        });
        yield connection.query(`create table ${tableName} (mainTitle VARCHAR(255), subTitle VARCHAR(255), mainText VARCHAR(255), subText VARCHAR(255), category VARCHAR(255), 
                            createdAt VARCHAR(255), readTime VARCHAR(255), img VARCHAR(255), indexStorie VARCHAR(255), title VARCHAR(255), tableName VARCHAR(255))`, function (error, result, fields) {
            if (error) {
                errMsg = error.sqlMessage;
                errCode += 'create table';
                isCreated = false;
                console.debug('createStorie table error, ' + error.sqlMessage);
            }
            ;
            console.debug('createStorie table res', result);
            // response = result
            // connected!
        });
        // await connection.query(`update ${title} SET mainTitle='${mainTitleRows['0']}', subTitle='${subTitleRows['0']}', mainText='${mainTextRows['0']}', subText='${subTextRows['0']}', category='${category}', createdAt='${createdAt}', readTime='${readTime}', img='${img}' where title = '${title}'`, await async function (error:any, result:any, fields:any) {
        //     if (error){ 
        //       errMsg = error.sqlMessage;
        //       errCode += 'update into';
        //       isCreated = false
        //       console.debug('updateStorie update error, ' + error.sqlMessage) 
        //     };
        //     console.debug('updateStorie update res',isCreated, result)  
        //     // connected!
        // });
        if (isCreated) {
            yield connection.query(`insert into ${tableName} (mainTitle, subTitle, mainText, subText, category, createdAt, readTime, img, indexStorie, title, tableName) values 
                              ('${mainTitleRows['0']}','${subTitleRows['0']}','${mainTextRows['0']}','${subTextRows['0']}','${category}','${createdAt}','${readTime}','${img}','${index}','${title}','${tableName}')`, function (error, result, fields) {
                if (error) {
                    errMsg = error.sqlMessage;
                    errCode += 'insert into';
                    isCreated = false;
                    console.debug('createStorie insert error, ' + error.sqlMessage);
                }
                ;
                console.debug('createStorie insert res', result);
                // connected!
            });
            for (let i = 1; i < Object.keys(mainTitleRows).length || i < Object.keys(subTitleRows).length || i < Object.keys(mainTextRows).length || i < Object.keys(subTextRows).length; i++) {
                let mtitle = '';
                let stitle = '';
                let mtext = '';
                let stext = '';
                if (i < Object.keys(mainTitleRows).length && mainTitleRows[`${i}`])
                    mtitle = mainTitleRows[`${i}`];
                if (i < Object.keys(subTitleRows).length && subTitleRows[`${i}`])
                    stitle = subTitleRows[`${i}`];
                if (i < Object.keys(mainTextRows).length && mainTextRows[`${i}`])
                    mtext = mainTextRows[`${i}`];
                if (i < Object.keys(subTextRows).length && subTextRows[`${i}`])
                    stext = subTextRows[`${i}`];
                yield connection.query(`insert into ${tableName} (mainTitle, subTitle, mainText, subText, category, createdAt, readTime, img, indexStorie, title, tableName) values 
                                  ('${mtitle}','${stitle}','${mtext}','${stext}','','','','','','','')`, function (error, result, fields) {
                    if (error) {
                        errMsg = error.sqlMessage;
                        errCode += 'insert into';
                        isCreated = false;
                        console.debug('updateStorie insert error, ' + error.sqlMessage);
                    }
                    ;
                    console.debug('updateStorie insert res', result);
                    // connected!
                });
                if (!isCreated)
                    break;
            }
        }
        yield connection.end(function (err) {
            if (err) {
                isCreated = false;
                res.status(400).json({ code: 400, msg: 'end connection error' });
                throw err;
            }
            ;
            console.debug('end connection updateStorie');
            if (errMsg === '')
                res.status(200).json({ code: 200, response: isCreated });
            else
                res.status(400).json({ code: 400, msg: 'updateStorie error, ' + errMsg, errCode: errCode });
        });
    }
    catch (error) {
        throw error;
    }
});
exports.updateStorie = updateStorie;
const insertInto = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let connection = yield connectDB(req, res, false);
        let errMsg = '';
        let errCode = 'i1$23i4-';
        let isCreated = true;
        let mainTitle, subTitle, mainText, subText, category, createdAt, readTime, img, index, title, tableName;
        console.debug('insertInto req', req.body);
        if (req.body && req.body.Storie_Data && req.body.Storie_Data[0]) {
            let data = req.body.Storie_Data[0];
            mainTitle = data.mainTitle;
            subTitle = data.subTitle;
            mainText = data.mainText;
            subText = data.subText;
            category = data.category;
            createdAt = data.createdAt;
            readTime = data.readTime;
            img = data.img;
            index = data.index;
            title = data.title;
            tableName = data.tableName;
        }
        yield connection.query(`insert into ${tableName} (mainTitle, subTitle, mainText, subText, category, createdAt, readTime, img, indexStorie, title, tableName) values 
      ('${mainTitle}','${subTitle}','${mainText}','${subText}','${category}','${createdAt}','${readTime}','${img}','${index}','${title}','${tableName}')`, function (error, result, fields) {
            if (error) {
                errMsg = error.sqlMessage;
                errCode += 'insert into';
                isCreated = false;
                console.debug('insertInto insert error, ' + error.sqlMessage);
            }
            ;
            console.debug('insertInto insert res', result);
            // connected!
        });
        yield connection.end(function (err) {
            if (err) {
                isCreated = false;
                res.status(400).json({ code: 400, msg: 'end connection error' });
                throw err;
            }
            ;
            console.debug('end connection insertInto');
            if (errMsg === '')
                res.status(200).json({ code: 200, response: isCreated });
            else
                res.status(400).json({ code: 400, msg: 'insertInto error, ' + errMsg, errCode: errCode });
        });
    }
    catch (error) {
        throw error;
    }
});
exports.insertInto = insertInto;
const createStorie = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let connection = yield connectDB(req, res, false);
        let errMsg = '';
        let errCode = 'c1$23s4-';
        let isCreated = true;
        let mainTitle, subTitle, mainText, subText, category, createdAt, readTime, img, index, title, tableName;
        console.debug('createStorie req', req.body);
        if (req.body && req.body.Storie_Data && req.body.Storie_Data[0]) {
            let data = req.body.Storie_Data[0];
            // mainTitle = data.mainTitle
            // subTitle = data.subTitle
            // mainText = data.mainText
            // subText = data.subText
            // category = data.category
            // createdAt = data.createdAt
            // readTime = data.readTime
            // img = data.img
            // index = data.index
            // title = data.title
            tableName = data.tableName;
        }
        yield connection.query(`create table ${tableName} (mainTitle VARCHAR(255), subTitle VARCHAR(255), mainText VARCHAR(255), subText VARCHAR(255), category VARCHAR(255), 
                            createdAt VARCHAR(255), readTime VARCHAR(255), img VARCHAR(255), indexStorie VARCHAR(255), title VARCHAR(255), tableName VARCHAR(255))`, function (error, result, fields) {
            if (error) {
                errMsg = error.sqlMessage;
                errCode += 'create table';
                isCreated = false;
                console.debug('createStorie table error, ' + error.sqlMessage);
            }
            ;
            console.debug('createStorie table res', result);
        });
        yield connection.end(function (err) {
            if (err) {
                isCreated = false;
                res.status(400).json({ code: 400, msg: 'end connection error' });
                throw err;
            }
            ;
            console.debug('end connection createStorie');
            if (errMsg === '')
                res.status(200).json({ code: 200, response: isCreated });
            else
                res.status(400).json({ code: 400, msg: 'createStorie error, ' + errMsg, errCode: errCode });
        });
    }
    catch (error) {
        throw error;
    }
});
exports.createStorie = createStorie;
