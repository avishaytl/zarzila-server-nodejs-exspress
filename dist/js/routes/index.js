"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const controllers = __importStar(require("../controllers/index"));
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();
const router = express_1.Router();
router.get("/connectDB", controllers.connectDB);
router.get("/verifyLogin", controllers.verifyLogin);
router.get("/getStoriesList", controllers.getStoriesList);
router.get("/getStorie", controllers.getStorie);
router.get("/getAdvertisements", controllers.getAdvertisements);
router.post("/createStorie", jsonParser, controllers.createStorie);
router.post("/insertInto", jsonParser, controllers.insertInto);
router.post("/updateStorie", jsonParser, controllers.updateStorie);
// router.post("/add-todo", addTodo)
// router.put("/edit-todo/:id", updateTodo)
// router.delete("/delete-todo/:id", deleteTodo)
exports.default = router;
