import { Router } from "express"
import * as controllers from "../controllers/index"
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()

const router: Router = Router()

router.get("/connectDB", controllers.connectDB)
router.get("/verifyLogin", controllers.verifyLogin)
router.get("/getStoriesList", controllers.getStoriesList)
router.get("/getStorie", controllers.getStorie)
router.get("/getAdvertisements", controllers.getAdvertisements)
router.post("/createStorie", jsonParser, controllers.createStorie)
router.post("/insertInto", jsonParser, controllers.insertInto)
router.post("/updateStorie", jsonParser, controllers.updateStorie)

// router.post("/add-todo", addTodo)

// router.put("/edit-todo/:id", updateTodo)

// router.delete("/delete-todo/:id", deleteTodo)

export default router