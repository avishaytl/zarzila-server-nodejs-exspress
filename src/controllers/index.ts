import { Response, Request } from "express"
import { Blog } from "./../types/blog" 

const connectDB = async (req: Request, res: Response, isRequest?: any): Promise<void | any> => {
  try {
    const mysql = require('mysql');  
    let connection = await mysql.createConnection({
        host     : `localhost`,
        user     : 'presolu3_admin',
        password : '400x120x',
        database : 'presolu3_zarzila'
    });
    
    await connection.connect(function(err: any) {
        if (err) {
        console.error('error connecting: ' + err.stack);
        // return;
        }

        console.log('connected as id ' + connection.threadId);
    });  

    if(isRequest)
      await connection.end(function(err:any) {
          if (err){
              res.status(400).json({code: 400, msg: 'end connection error'}) 
              throw err;
          }; 

          console.debug('end connection connectDB')  

          res.status(200).json({code: 200, response: 'The DB Is Ready To Use'}) 
      }); 
      else return connection;

  } catch (error) {
    throw error
  }
}

const verifyLogin = async (req: Request, res: Response): Promise<void> =>{
    try{ 

      let connection: any = await connectDB(req, res, false) 
      let user: any, pass: any;
      let errMsg = ''; 
      let errCode = 'v1$23l4-'; 
      let isVerifyed = false;
      for (const key in req.query) {  
        const element = req.query[key];
        if(key === 'user')
          user = element
        if(key === 'pass')
          pass = element 
      }

      console.debug('verifyLogin req',user,pass)

      if(pass && pass !== undefined && user && user !== undefined)
        await connection.query(`select userName from users where userName='` + user + `' and userPass='` + pass + `'`, function (error:any, result:any, fields:any) {
          if (error){ 
              errMsg = error.sqlMessage;
              errCode += 'select from'
              console.debug('verifyLogin error, ' + error.sqlMessage) 
          };
            if(result && result[0] && result[0].userName === user)
              isVerifyed = true;
            console.debug('verifyLogin res', result) 
            // connected!
        });

      await connection.end(function(err:any) {
          if (err){
              res.status(400).json({code: 400, msg: 'end connection error'}) 
              throw err;
          };

          console.debug('end connection verifyLogin') 

          if(errMsg === '')
            res.status(200).json({code: 200, response: isVerifyed}) 
          else
            res.status(400).json({code: 400, msg: 'verifyLogin error, ' + errMsg, errCode: errCode}) 
      });   

    }catch(error){
      throw error;
    }
}

const getAdvertisements = async (req: Request, res: Response): Promise<void> =>{
    try{ 

      let connection: any = await connectDB(req, res, false) 
      let user: any, pass: any;
      let errMsg = ''; 
      let errCode = '1$23a4-';  
      let isCreated = true;
      let resData:any = []; 

        await connection.query(`select * from advertisements`, function (error:any, result:any, fields:any) {
          if (error){ 
              errMsg = error.sqlMessage;
              errCode += 'select from'
              isCreated = false;
              console.debug('getAdvertisements error, ' + error.sqlMessage) 
          }; 
            console.debug('getAdvertisements res', result) 
            if(result)
              resData.push(result)
            // connected!
        });

      await connection.end(function(err:any) {
          if (err){
              res.status(400).json({code: 400, msg: 'end connection error'}) 
              throw err;
          };

          console.debug('end connection getAdvertisements') 

          if(errMsg === '')
            res.status(200).json({code: 200, response: isCreated, data: resData}) 
          else
            res.status(400).json({code: 400, msg: 'getAdvertisements error, ' + errMsg, errCode: errCode}) 
      });   

    }catch(error){
      throw error;
    }
}

const getStorie = async (req: Request, res: Response): Promise<void> =>{
  try{ 

    let connection: any = await connectDB(req, res, false) 
    let errMsg = ''; 
    let isCreated = true;
    let errCode = 'g1$23sd4-';  
    let resData:any = []; 
    let tables:any = [];
    let tableName:any = '';
    for (const key in req.query) {  
      const element = req.query[key];
      if(key === 'tableName')
        tableName = element 
    }

    console.debug('getStorie req',tableName)  

      await connection.query(`select * from ${tableName}`,function (error:any, result:any, fields:any) {
        if (error){ 
            errMsg = error.sqlMessage;
            isCreated = false
            errCode += 'select from'
            console.debug('getStorie error, ' + error) 
        }; 

          console.debug('getStorie res', result) 
          if(result)
            resData.push(result)
          // connected!
      }); 

      console.debug('resData',resData) 


    await connection.end(function(err:any) {
        if (err){
            res.status(400).json({code: 400, msg: 'end connection error'}) 
            throw err;
        };

        console.debug('end connection getStorie') 

        if(errMsg === '')
          res.status(200).json({code: 200, response: isCreated, data: resData}) 
        else 
          res.status(400).json({code: 400, msg: 'getStorie error, ' + errMsg, errCode: errCode}) 
    });   
  }catch(error){
    throw error;
  }

}

const getStoriesList = async (req: Request, res: Response): Promise<void> =>{
    try{ 

      let connection: any = await connectDB(req, res, false) 
      let errMsg = ''; 
      let isCreated = true;
      let errCode = 'g1$23sd4-';  
      let resData:any = []; 
      let tables:any = [];

      console.debug('getStoriesData req')  

     await connection.query(`SELECT table_name FROM information_schema.tables WHERE table_schema ='presolu3_zarzila'`,function (error:any, result:any, fields:any) {
          if (error){ 
            errMsg = error.sqlMessage;
            errCode += 'select from'
            isCreated = false
            console.debug('getStoriesData error, ' + error.sqlMessage) 
          };
          console.debug('getStoriesData res', result)  
          if(result)
            tables = result; 

          // connected! 
      });


      await connection.end(function(err:any) {
          if (err){
              res.status(400).json({code: 400, msg: 'end connection error'}) 
              throw err;
          };

          console.debug('end connection getStoriesData')
          let respose = [];
          // let response: Blog[] = [{
          //     title: 'note title',
          //     description: 'note description' 
          // }]
          for(let i = 0;i < tables.length;i++)
            if(tables[i].table_name !== 'users' && tables[i].table_name !== 'advertisements')
              respose.push(tables[i])
          if(errMsg === '')
            res.status(200).json({code: 200, response: isCreated, data: respose}) 
          else 
            res.status(400).json({code: 400, msg: 'getStoriesData error, ' + errMsg, errCode: errCode}) 
      });   
    }catch(error){
      throw error;
    }
}

const updateStorie = async (req: Request, res: Response): Promise<void> =>{
  try{  

    let connection: any = await connectDB(req, res, false) 
    let errMsg = ''; 
    let errCode = 'u1$23s4-'; 
    let isCreated = true;
    let mainTitle, subTitle, mainText, subText, category, createdAt, readTime, img, index, title, tableName;

    console.debug('updateStorie req',req.body) 

    if(req.body && req.body.Storie_Data && req.body.Storie_Data[0]){
        let data = req.body.Storie_Data[0];
        mainTitle = data.mainTitle.split("_jg_").join('"')
        subTitle = data.subTitle.split("_jg_").join('"')
        mainText = data.mainText.split("_jg_").join('"')
        subText = data.subText.split("_jg_").join('"')
        category = data.category
        createdAt = data.createdAt
        readTime = data.readTime
        img = data.img
        title = data.title
        index = data.index;
        tableName = data.tableName;
    } 
    let mainTitleRows = await JSON.parse(mainTitle)
    let subTitleRows = await JSON.parse(subTitle)
    let mainTextRows = await JSON.parse(mainText)
    let subTextRows = await JSON.parse(subText)  
    let response;
    
    await connection.query(`DROP TABLE ${tableName}`, await async function (error:any, result:any, fields:any) {
        if (error){ 
          errMsg = error.sqlMessage;
          errCode += 'update into';
          isCreated = false
          console.debug('updateStorie update error, ' + error.sqlMessage) 
        };
        console.debug('updateStorie update res',isCreated, result)  
        // connected!
    });

    await connection.query(`create table ${tableName} (mainTitle VARCHAR(255), subTitle VARCHAR(255), mainText VARCHAR(255), subText VARCHAR(255), category VARCHAR(255), 
                            createdAt VARCHAR(255), readTime VARCHAR(255), img VARCHAR(255), indexStorie VARCHAR(255), title VARCHAR(255), tableName VARCHAR(255))`,function (error:any, result:any, fields:any) {
        if (error){ 
          errMsg = error.sqlMessage;
          errCode += 'create table';
          isCreated = false
          console.debug('createStorie table error, ' + error.sqlMessage)   
        };
        console.debug('createStorie table res', result) 
        // response = result
        // connected!
    });
    // await connection.query(`update ${title} SET mainTitle='${mainTitleRows['0']}', subTitle='${subTitleRows['0']}', mainText='${mainTextRows['0']}', subText='${subTextRows['0']}', category='${category}', createdAt='${createdAt}', readTime='${readTime}', img='${img}' where title = '${title}'`, await async function (error:any, result:any, fields:any) {
    //     if (error){ 
    //       errMsg = error.sqlMessage;
    //       errCode += 'update into';
    //       isCreated = false
    //       console.debug('updateStorie update error, ' + error.sqlMessage) 
    //     };
    //     console.debug('updateStorie update res',isCreated, result)  
    //     // connected!
    // });

    if(isCreated){ 
      await connection.query(`insert into ${tableName} (mainTitle, subTitle, mainText, subText, category, createdAt, readTime, img, indexStorie, title, tableName) values 
                              ('${mainTitleRows['0']}','${subTitleRows['0']}','${mainTextRows['0']}','${subTextRows['0']}','${category}','${createdAt}','${readTime}','${img}','${index}','${title}','${tableName}')`, function (error:any, result:any, fields:any) {
          if (error){ 
            errMsg = error.sqlMessage;
            errCode += 'insert into';
            isCreated = false
            console.debug('createStorie insert error, ' + error.sqlMessage) 
          };
          console.debug('createStorie insert res', result) 
          // connected!
      });
      for(let i = 1;i < Object.keys(mainTitleRows).length || i < Object.keys(subTitleRows).length || i < Object.keys(mainTextRows).length || i < Object.keys(subTextRows).length;i++){
          let  mtitle = ''
          let  stitle = ''
          let  mtext = ''
          let  stext = ''
          if(i < Object.keys(mainTitleRows).length && mainTitleRows[`${i}`])
            mtitle = mainTitleRows[`${i}`];
          if(i < Object.keys(subTitleRows).length && subTitleRows[`${i}`])
            stitle = subTitleRows[`${i}`];
          if(i < Object.keys(mainTextRows).length && mainTextRows[`${i}`])
            mtext =  mainTextRows[`${i}`];
          if(i < Object.keys(subTextRows).length && subTextRows[`${i}`])
            stext = subTextRows[`${i}`]; 
          await connection.query(`insert into ${tableName} (mainTitle, subTitle, mainText, subText, category, createdAt, readTime, img, indexStorie, title, tableName) values 
                                  ('${mtitle}','${stitle}','${mtext}','${stext}','','','','','','','')`, function (error:any, result:any, fields:any) {
              if (error){ 
                errMsg = error.sqlMessage;
                errCode += 'insert into';
                isCreated = false
                console.debug('updateStorie insert error, ' + error.sqlMessage) 
              };
              console.debug('updateStorie insert res', result) 
              // connected!
          });  
          if(!isCreated)
            break;
      }  
    }  

    await connection.end(function(err:any) {
        if (err){
            isCreated = false
            res.status(400).json({code: 400, msg: 'end connection error'}) 
            throw err;
        };

        console.debug('end connection updateStorie') 

        if(errMsg === '')
          res.status(200).json({code: 200, response: isCreated}) 
        else 
          res.status(400).json({code: 400, msg: 'updateStorie error, ' + errMsg, errCode: errCode}) 
    });   

  }catch(error){
    throw error;
  } 
}
const insertInto = async (req: Request, res: Response): Promise<void> =>{
  try{ 

    let connection: any = await connectDB(req, res, false) 
    let errMsg = ''; 
    let errCode = 'i1$23i4-'; 
    let isCreated = true;
    let mainTitle:any, subTitle:any, mainText:any, subText:any, category:any, createdAt:any, readTime:any, img:any, index:any, title:any, tableName:any;
    console.debug('insertInto req',req.body) 
    if(req.body && req.body.Storie_Data && req.body.Storie_Data[0]){
        let data = req.body.Storie_Data[0];
        mainTitle = data.mainTitle
        subTitle = data.subTitle
        mainText = data.mainText
        subText = data.subText
        category = data.category
        createdAt = data.createdAt
        readTime = data.readTime
        img = data.img
        index = data.index
        title = data.title
        tableName = data.tableName
    } 
    
    await connection.query(`insert into ${tableName} (mainTitle, subTitle, mainText, subText, category, createdAt, readTime, img, indexStorie, title, tableName) values 
      ('${mainTitle}','${subTitle}','${mainText}','${subText}','${category}','${createdAt}','${readTime}','${img}','${index}','${title}','${tableName}')`, function (error:any, result:any, fields:any) {
      if (error){ 
        errMsg = error.sqlMessage;
        errCode += 'insert into';
        isCreated = false
      console.debug('insertInto insert error, ' + error.sqlMessage) 
      };
      console.debug('insertInto insert res', result) 
      // connected!
    });  

    await connection.end(function(err:any) {
        if (err){
            isCreated = false
            res.status(400).json({code: 400, msg: 'end connection error'}) 
            throw err;
        };

        console.debug('end connection insertInto') 

        if(errMsg === '')
          res.status(200).json({code: 200, response: isCreated}) 
        else 
          res.status(400).json({code: 400, msg: 'insertInto error, ' + errMsg, errCode: errCode}) 
    });   

  }catch(error){
    throw error;
  }
} 

const createStorie = async (req: Request, res: Response): Promise<void> =>{
  try{ 

    let connection: any = await connectDB(req, res, false) 
    let errMsg = ''; 
    let errCode = 'c1$23s4-'; 
    let isCreated = true;
    let mainTitle:any, subTitle:any, mainText:any, subText:any, category:any, createdAt:any, readTime:any, img:any, index:any, title:any, tableName:any;
    console.debug('createStorie req',req.body) 
    if(req.body && req.body.Storie_Data && req.body.Storie_Data[0]){
        let data = req.body.Storie_Data[0];
        // mainTitle = data.mainTitle
        // subTitle = data.subTitle
        // mainText = data.mainText
        // subText = data.subText
        // category = data.category
        // createdAt = data.createdAt
        // readTime = data.readTime
        // img = data.img
        // index = data.index
        // title = data.title
        tableName = data.tableName
    }
    
    await connection.query(`create table ${tableName} (mainTitle VARCHAR(255), subTitle VARCHAR(255), mainText VARCHAR(255), subText VARCHAR(255), category VARCHAR(255), 
                            createdAt VARCHAR(255), readTime VARCHAR(255), img VARCHAR(255), indexStorie VARCHAR(255), title VARCHAR(255), tableName VARCHAR(255))`,function (error:any, result:any, fields:any) {
        if (error){ 
          errMsg = error.sqlMessage;
          errCode += 'create table';
          isCreated = false
          console.debug('createStorie table error, ' + error.sqlMessage) 
        };
        console.debug('createStorie table res', result)  
    });  

    await connection.end(function(err:any) {
        if (err){
            isCreated = false
            res.status(400).json({code: 400, msg: 'end connection error'}) 
            throw err;
        };

        console.debug('end connection createStorie') 

        if(errMsg === '')
          res.status(200).json({code: 200, response: isCreated}) 
        else 
          res.status(400).json({code: 400, msg: 'createStorie error, ' + errMsg, errCode: errCode}) 
    });   

  }catch(error){
    throw error;
  }
}

export { connectDB, verifyLogin, getStoriesList, createStorie, updateStorie, getStorie, insertInto, getAdvertisements }