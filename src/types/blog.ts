export interface Blog {
    title: string
    description: string
    images?: string []
}